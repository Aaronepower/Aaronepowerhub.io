var links = [ document.getElementById('education_link')
            , document.getElementById('source_link')
            , document.getElementById('contact_link')
            ]

for (var i = 0; i < links.length; i++) {
	links[i].addEventListener('click', bringUpText)
}

function bringUpText() {
	for (var i = 0; i < links.length; i++) {
		links[i].style.borderBottom = 'none'

		var content = links[i].id.split('_')[0]
		document.getElementById(content).style.display = 'none'
	}
	this.style.borderBottom = '3px solid white'
	document.getElementById(this.id.split('_')[0]).style.display = 'initial'
}


document.getElementById('send_button').addEventListener('click', function (event) {
	event.preventDefault()

	var email = document.getElementById('email').value
	, subject = document.getElementById('subject').value
	, body = document.getElementById('body').value
	var regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g
	if (email.match(regex) && subject && body) {
		var request = new XMLHttpRequest();
        request.open('POST', 'https://mandrillapp.com/api/1.0/messages/send.json', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onload = function () {
        	document.getElementById('contact').innerHTML = '<h1>Sent!</h1>'
        }
        request.send({
		key: 'ME3TKvhAhLF5VPdEBTuLDA',
		message: {
			from_email: escape(email),
			to: [
			{
				'email': 'theaaronepower\@gmail\.com',
				'name': 'Aaron\ Power',
				'type': 'to'
			}
			],
			autotext: true,
			subject: escape(subject),
			html: escape(body)
		}
	    })
	}
})
