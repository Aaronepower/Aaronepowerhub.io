const gulp   = require('gulp')
    , minJS  = require('gulp-uglify')
    , concat = require('gulp-concat')
    , minCSS = require('gulp-minify-css')
    , del    = require('del')
    , wrap   = require('gulp-wrap')
    , minIMG = require('gulp-imagemin')

gulp.task('scripts', function () {
    del.sync('./script.min.js')
	return gulp.src('./script.js')
		   .pipe(concat('script.min.js'))
		   .pipe(wrap('(function (){\n "use strict";\n <%= contents %>\n})();'))
		   .pipe(minJS())
		   .pipe(gulp.dest('./'))
})

gulp.task('styles', function() {
        del.sync('./style.min.css')
	return gulp.src('./*.css')
			   .pipe(concat('style.min.css'))
			   .pipe(minCSS())
			   .pipe(gulp.dest('./'))
})

gulp.task('images', function() {
    del.sync('./img')
	return gulp.src(['./bg.jpg', 'twitter_image.jpg'])
			   .pipe(minIMG())
			   .pipe(gulp.dest('./img'))
})

gulp.task('default', ['scripts', 'styles', 'images'])
